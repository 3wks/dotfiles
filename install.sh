#!/usr/bin/env bash
set -e

# Prettify the console log
GREEN="$(tput setaf 2)"
RESET="$(tput sgr0)"
log() {
    echo "${GREEN}dotfiles:${RESET} ${1}"
    for var in "${@:2}"
    do
        echo "${GREEN}          ${var}${RESET}"
    done
}

install_if_not_present() {
    local cmd=${1}
    shift;

    if [ -x "$(command -v ${cmd})" ]; then
       log "✅ ${cmd} already installed"
    else
        log "Installing ${cmd}..."
        "$@"
        log "${cmd} installed 👌"
    fi
}

install () {
    install_if_not_present "brew" /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

    log "Updating Homebrew..."
    brew update

    install_if_not_present "brew cask" brew tap caskroom/cask
    
    install_if_not_present "git" brew install git

    if [ $(/usr/libexec/java_home -v 1.8) ]; then
       log "✅ java already installed"
    else
        log "Installing java 8..."
        brew tap caskroom/versions
        brew cask install java8
        log "java 8 installed 👌"
    fi

    install_if_not_present "mvn" brew install maven

    install_if_not_present "node" brew install node

    install_if_not_present "yo" npm i -g yo

    log "Installing Yeoman generator"
    npm i -g generator-thundr-gae-react

    log "All done 🎉🎉🎉"
}

install
