# 3wks dotfiles (alpha)

Automate some of the initial setup from this video: https://www.youtube.com/watch?v=Rcwyp-vkhCg

### Install script

Run this in a terminal

```sh
curl -o- https://bitbucket.org/3wks/dotfiles/raw/master/install.sh | bash
```
